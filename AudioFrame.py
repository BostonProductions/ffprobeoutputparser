__author__ = 'Emily Charles'

from Frame import Frame
import PacketDatumLookup

class AudioFrame(Frame):
    parentClass = Frame
    sample_fmt = ""
    nb_samples = -1
    channels = -1
    channel_layout = ""

    def getSetterFromKey(self, key):
         if key in PacketDatumLookup.common_parameters:
             return self.parentClass.getSetterFromKey(self, key)
         else:
             if key == PacketDatumLookup.KEY_SAMPLE_FMT:
                return self.set_sample_fmt
             elif key == PacketDatumLookup.KEY_NB_SAMPLES:
                return self.set_nb_samples
             elif key == PacketDatumLookup.KEY_CHANNELS:
                return self.set_channels
             elif key == PacketDatumLookup.KEY_CHANNEL_LAYOUT:
                return self.set_channel_layout

    def getGetterFromKey(self, key):
         if key in PacketDatumLookup.common_parameters:
             return self.parentClass.getGetterFromKey(self, key)
         else:
             if key == PacketDatumLookup.KEY_SAMPLE_FMT:
                return self.get_sample_fmt
             elif key == PacketDatumLookup.KEY_NB_SAMPLES:
                return self.get_nb_samples
             elif key == PacketDatumLookup.KEY_CHANNELS:
                return self.get_channels
             elif key == PacketDatumLookup.KEY_CHANNEL_LAYOUT:
                return self.get_channel_layout

    def __str__(self):
        s = self.start_packet()
        s += str(self.parentClass)
        class_attrs = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self,a))]
        for item in class_attrs:
            s += item + " = " + str(getattr(self, item)) + " \n"

        s += self.end_packet()
        return s

############### SETTERS ###############
    def set_sample_fmt(self, value):
        self.sample_fmt = value

    def set_nb_samples(self, value):
        self.nb_samples = value

    def set_channels(self, value):
        self.channels = value

    def set_channel_layout(self, value):
        self.channel_layout = value



############### GETTERS ###############

    def get_sample_fmt(self):
        return self.sample_fmt

    def get_nb_samples(self):
        return self.nb_samples

    def get_channels(self):
        return self.channels

    def get_channel_layout(self):
        return self.channel_layout



