__author__ = 'Emily Charles'

import PacketDatumLookup


class Frame:
    media_type = ""
    stream_index = -1
    key_frame = -1
    pkt_pts = -1
    pkt_pts_time = -1
    pkt_dts = -1
    pkt_dts_time = -1
    best_effort_timestamp = -1
    best_effort_timestamp_time = -1
    pkt_duration = -1
    pkt_duration_time = ""
    pkt_pos = -1
    pkt_size = -1




    def __init__(self):
        pass

    def getSetterFromKey(self, key):
        if key == PacketDatumLookup.KEY_MEDIA_TYPE:
            return self.set_media_type
        elif key == PacketDatumLookup.KEY_STREAM_INDEX:
            return self.set_stream_index
        elif key == PacketDatumLookup.KEY_FRAME:
            return self.set_key_frame
        elif key == PacketDatumLookup.KEY_PKT_PTS:
            return self.set_pkt_pts
        elif key == PacketDatumLookup.KEY_PKT_PTS_TIME:
            return self.set_pts_time
        elif key == PacketDatumLookup.KEY_PKT_DTS:
            return self.set_pkt_dts
        elif key == PacketDatumLookup.KEY_PKT_DTS_TIME:
            return self.set_dts_time
        elif key == PacketDatumLookup.KEY_BEST_EFFORT_TIMESTAMP:
            return self.set_best_effort_timestamp
        elif key == PacketDatumLookup.KEY_BEST_EFFORT_TIMESTAMP_TIME:
            return self.set_best_effort_timestamp_time
        elif key == PacketDatumLookup.KEY_PKT_DURATION:
            return self.set_pkt_duration
        elif key == PacketDatumLookup.KEY_PKT_DURATION_TIME:
            return self.set_pkt_duration_time
        elif key == PacketDatumLookup.KEY_PKT_POS:
            return self.set_pkt_pos
        elif key == PacketDatumLookup.KEY_PKT_SIZE:
            return self.set_pkt_size

    def getGetterFromKey(self, key):
        if key == PacketDatumLookup.KEY_MEDIA_TYPE:
            return self.get_media_type
        elif key == PacketDatumLookup.KEY_STREAM_INDEX:
            return self.get_stream_index
        elif key == PacketDatumLookup.KEY_FRAME:
            return self.get_key_frame
        elif key == PacketDatumLookup.KEY_PKT_PTS:
            return self.get_pkt_pts
        elif key == PacketDatumLookup.KEY_PKT_PTS_TIME:
            return self.get_pts_time
        elif key == PacketDatumLookup.KEY_PKT_DTS:
            return self.get_pkt_dts
        elif key == PacketDatumLookup.KEY_PKT_DTS_TIME:
            return self.get_dts_time
        elif key == PacketDatumLookup.KEY_BEST_EFFORT_TIMESTAMP:
            return self.get_best_effort_timestamp
        elif key == PacketDatumLookup.KEY_BEST_EFFORT_TIMESTAMP_TIME:
            return self.get_best_effort_timestamp_time
        elif key == PacketDatumLookup.KEY_PKT_DURATION:
            return self.get_pkt_duration
        elif key == PacketDatumLookup.KEY_PKT_DURATION_TIME:
            return self.get_pkt_duration_time
        elif key == PacketDatumLookup.KEY_PKT_POS:
            return self.get_pkt_pos
        elif key == PacketDatumLookup.KEY_PKT_SIZE:
            return self.get_pkt_size


    def __str__(self):
        class_attrs = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self,a))]
        s = "\n"
        for item in class_attrs:
            s += item + " = " + str(getattr(self, item)) + " \n"
        return s

    def start_packet(self):
        return "START OF PACKET"

    def end_packet(self):
        return "*************** END OF PACKET ***************"


    ############### SETTERS ###############
    def set_media_type(self, value):
        self.media_type = value;

    def set_stream_index(self, value):
        self.stream_index = value;

    def set_key_frame(self, value):
        self.key_frame = value

    def set_pkt_pts(self, value):
        self.pkt_pts = value

    def set_pts_time(self, value):
        self.pts_time = value

    def set_pkt_dts(self, value):
        self.pkt_dts = value

    def set_dts_time(self, value):
        self.dts_time = value

    def set_best_effort_timestamp(self, value):
        self.best_effort_timestamp = value

    def set_best_effort_timestamp_time(self, value):
        self.best_effort_timestamp_time = value

    def set_pkt_duration(self, value):
        self.pkt_duration = value

    def set_pkt_duration_time(self, value):
        self.pkt_duration_time = value

    def set_pkt_pos(self, value):
        self.pkt_pos = value

    def set_pkt_size(self, value):
        self.pkt_size = value

    ############### GETTERS ###############
    def get_media_type(self):
        return self.media_type

    def get_stream_index(self):
        return self.stream_index

    def get_key_frame(self):
        return self.key_frame

    def get_pkt_pts(self):
        return self.pkt_pts

    def get_pts_time(self):
        return self.pts_time

    def get_pkt_dts(self):
        return self.pkt_dts

    def get_dts_time(self):
        return self.pkt_dts_time

    def get_best_effort_timestamp(self):
        return self.best_effort_timestamp

    def get_best_effort_timestamp_time(self):
        return self.best_effort_timestamp_time

    def get_pkt_duration(self):
        return self.pkt_duration

    def get_pkt_duration_time(self):
        return self.pkt_duration_time

    def get_pkt_pos(self):
        return self.pkt_pos

    def get_pkt_size(self):
        return self.pkt_size


